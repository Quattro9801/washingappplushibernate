package com.mycompany.hibernate.many_to_many.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "sections")
public class Section {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name_section")
    private String secName;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "child_section",
        joinColumns = @JoinColumn(name = "sec_id"),// какой столбец из таблицы child_sec соответствует нашей таблицы
        inverseJoinColumns = @JoinColumn(name = "child_id"))
    private List<Child> children;

    public Section(String secName) {
        this.secName = secName;
    }

    public Section() {
    }

    public String getSecName() {
        return secName;
    }

    public void setSecName(String secName) {
        this.secName = secName;
    }
    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }
    public void addChildToChildren(Child child){
        if(children == null){
            children = new ArrayList<>();
        }
        children.add(child);
    }

    @Override
    public String toString() {
        return "Section{" +
            "id=" + id +
            ", secName='" + secName + '\'' +
            '}';
    }
}
