package com.mycompany.hibernate.many_to_many.test;

import com.mycompany.hibernate.many_to_many.entity.Child;
import com.mycompany.hibernate.many_to_many.entity.Section;
import com.mycompany.hibernate.one_to_many.modelBiDirect.Order;
import com.mycompany.hibernate.one_to_many.modelBiDirect.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class Test {
    public static void main(String[] args) {
        SessionFactory sessionFactory = new AnnotationConfiguration()// создали фабрику, создаем 1 раз
            .configure("hibernate.cfg.xml")
            .addAnnotatedClass(Child.class)
            .addAnnotatedClass(Section.class)
            .buildSessionFactory();
        try{
            Session session = sessionFactory.getCurrentSession();
            session.beginTransaction();
            Child child = (Child) session.get(Child.class,1);
            System.out.println(child);
            System.out.println("//////////////////////////////");
            System.out.println(child.getSections());
            session.getTransaction().commit();

        }finally {
            sessionFactory.close();

        }
    }
}
