package com.mycompany.hibernate.washing;

import com.mycompany.hibernate.washing.entity.Car;
import com.mycompany.hibernate.washing.entity.CarWash;
import com.mycompany.hibernate.washing.entity.Location;
import com.mycompany.hibernate.washing.entity.ModelCar;
import com.mycompany.hibernate.washing.entity.Order;
import com.mycompany.hibernate.washing.entity.OrderItem;
import com.mycompany.hibernate.washing.entity.Review;
import com.mycompany.hibernate.washing.entity.Service;
import com.mycompany.hibernate.washing.entity.TypeCar;
import com.mycompany.hibernate.washing.entity.User;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.stereotype.Component;

@Component("factoryBuilder")
public class FactoryBuilder {

    private final SessionFactory sessionFactory;

    public FactoryBuilder() {
       sessionFactory = new AnnotationConfiguration()
            .configure("hibernate.cfg.xml")
            .addAnnotatedClass(User.class)
            .addAnnotatedClass(Order.class)
            .addAnnotatedClass(OrderItem.class)
            .addAnnotatedClass(Service.class)
            .addAnnotatedClass(Car.class)
            .addAnnotatedClass(CarWash.class)
            .addAnnotatedClass(TypeCar.class)
            .addAnnotatedClass(Review.class)
            .addAnnotatedClass(Location.class)
            .addAnnotatedClass(ModelCar.class)
           .buildSessionFactory();
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
