package com.mycompany.hibernate.washing.implimentation;

import com.mycompany.hibernate.washing.DAO.ServiceDao;
import com.mycompany.hibernate.washing.FactoryBuilder;
import com.mycompany.hibernate.washing.entity.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component("serviceDaoImpl")
public class ServiceDaoImpl implements ServiceDao {

    @Autowired
    FactoryBuilder factoryBuilder;

    @Override
    public Service addService(Service service) {
        return null;
    }

    @Override
    public void deleteService(int id) {

    }

    @Override
    public Service updateService(int id, Service service) {
        return null;
    }

    @Override
    public List<Service> getAllServices() {
        return null;
    }

    @Override
    public Service getServiceById(int id) {
        return null;
    }
}
