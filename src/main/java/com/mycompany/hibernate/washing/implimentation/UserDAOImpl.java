package com.mycompany.hibernate.washing.implimentation;

import com.mycompany.hibernate.washing.DAO.UserDao;
import com.mycompany.hibernate.washing.FactoryBuilder;
import com.mycompany.hibernate.washing.entity.Car;
import com.mycompany.hibernate.washing.entity.Order;
import com.mycompany.hibernate.washing.entity.User;
import org.hibernate.Session;;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Component("userDaoImpl")
public class UserDAOImpl implements UserDao {

    @Autowired
    FactoryBuilder factoryBuilder;

    private UserDAOImpl() {
    }

    @Override
    public User addUser(User user) {
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(user);
            System.out.println("Added user" + user);
            session.getTransaction().commit();
        } finally {
            factoryBuilder.getSessionFactory().close();

        }
        return user;
    }

    @Override
    public String removeUser(int id) {
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            User user = (User) session.get(User.class, id);
            session.delete(user);
            System.out.println("Deleted User " + user);
            session.getTransaction().commit();
        } finally {
            factoryBuilder.getSessionFactory().close();

        }
        return "Success";
    }

    @Override
    public User findByUserId(int id) {
        User user = null;
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            user = (User) session.get(User.class, id);
            System.out.println("FindUserByID " + user);
            session.getTransaction().commit();

        } finally {
            factoryBuilder.getSessionFactory().close();

        }
        return user;
    }

    @Override
    public User updateUserById(int id, User updatedUser) {
            User user = null;
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            user = (User) session.get(User.class, id);
            user.setName(updatedUser.getName());
            user.setEmail(updatedUser.getEmail());
            user.setOrders(new ArrayList<Order>());
            user.setCars( new ArrayList<Car>());
            //session.save(user);
            session.getTransaction().commit();
            System.out.println("User updated success" + user);

        } finally {
            factoryBuilder.getSessionFactory().close();

        }
        return user;
    }

    @Override
    public List<User> getAllUsers() {
        List<User> userList = null;

        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            userList = session.createQuery("FROM User").list();
        } finally {
            factoryBuilder.getSessionFactory().close();
        }
        return userList;
    }

    @Override
    public List<Order> getAllOrdersByUser(BigInteger userId) {
        List<Order> orderList;
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            User user = (User) session.get(User.class, userId);
            orderList = user.getOrders();
            session.getTransaction().commit();
            System.out.println("All orders was gotten By user" + user);

        } finally {
            factoryBuilder.getSessionFactory().close();

        }
        return orderList;
    }
}
