package com.mycompany.hibernate.washing.implimentation;

import com.mycompany.hibernate.washing.DAO.CarWashDao;
import com.mycompany.hibernate.washing.FactoryBuilder;
import com.mycompany.hibernate.washing.entity.CarWash;
import org.springframework.beans.factory.annotation.Autowired;

public class CarWashDaoImpl implements CarWashDao {

    @Autowired
    FactoryBuilder factoryBuilder;

    public CarWashDaoImpl() {
    }

    @Override
    public void createCarWash(CarWash carWash) {

    }

    @Override
    public CarWash getCarWashById(int id) {
        return null;
    }

    @Override
    public CarWash updateCarWash(int id, CarWash carWash) {
        return null;
    }

    @Override
    public void deleteCarWash(int id) {

    }
}
