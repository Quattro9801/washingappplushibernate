package com.mycompany.hibernate.washing.implimentation;

import com.mycompany.hibernate.washing.DAO.CarDao;
import com.mycompany.hibernate.washing.FactoryBuilder;
import com.mycompany.hibernate.washing.entity.Car;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("carDaoImpl")
public class CarDaoImpl implements CarDao {

    @Autowired
    FactoryBuilder factoryBuilder;

    public CarDaoImpl() {
    }

    @Override
    public void createCar(Car car) {
        try {
            Session session =factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(car);
            System.out.println("Car was created" + car);
            session.getTransaction().commit();
        } finally {
            factoryBuilder.getSessionFactory().close();

        }
    }

    @Override
    public void deleteCar(int id) {
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Car car = (Car) session.get(Car.class, id);
            session.delete(car);
            System.out.println("Car was deleted " + car);
            session.getTransaction().commit();
        } finally {
            factoryBuilder.getSessionFactory().close();

        }

    }

    @Override
    public Car updateCar(int id, Car c) {
        Car car = null;
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            car = (Car) session.get(Car.class, id);
            car.setCarName(c.getCarName());
            session.save(car);
            System.out.println("Car " + car);
            session.getTransaction().commit();
        } finally {
            factoryBuilder.getSessionFactory().close();

        }
        return car;

    }

    @Override
    public Car getCarById(int id) {
        Car car = null;
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            car = (Car) session.get(Car.class, id);
            System.out.println("Car " + car);
            session.getTransaction().commit();
        } finally {
            factoryBuilder.getSessionFactory().close();
        }
     return car;
    }
}
