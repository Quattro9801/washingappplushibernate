package com.mycompany.hibernate.washing.implimentation;

import com.mycompany.hibernate.washing.DAO.OrderDao;
import com.mycompany.hibernate.washing.FactoryBuilder;
import com.mycompany.hibernate.washing.entity.Car;
import com.mycompany.hibernate.washing.entity.Order;
import com.mycompany.hibernate.washing.entity.OrderItem;
import com.mycompany.hibernate.washing.entity.User;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Component("orderDaoImpl")
public class OrderDaoImpl implements OrderDao {
    @Autowired
    FactoryBuilder factoryBuilder;
    public OrderDaoImpl() {
    }

    @Override
    public void createOrder(Order order) {
        try {
            Session session =factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(order);
            System.out.println("SO was created" + order);
            session.getTransaction().commit();
        } finally {
            factoryBuilder.getSessionFactory().close();

        }
    }

    @Override
    public Order getOrderById(BigInteger id) {
        Order order;
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            order = (Order) session.get(Order.class, id);
            System.out.println("GetOrderByID " + order );
            session.getTransaction().commit();

        } finally {
            factoryBuilder.getSessionFactory().close();

        }
        return order;
    }

    @Override
    public Order updateOrder(BigInteger id, Order updatedOrder) {
        Order order;
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            order = (Order) session.get(Order.class, id);
            if (order == null) {
                System.out.println("Order with id not found" + id);
                return null;
            }
           order.setStatus(updatedOrder.getStatus());
            session.save(order);
            session.getTransaction().commit();

        } finally {
            factoryBuilder.getSessionFactory().close();

        }

        return order;
    }

    @Override
    public void addOrderItemToSalesOrder(BigInteger orderId, OrderItem orderItem) {
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Order order = (Order) session.get(Order.class, orderId);
            if (order == null) {
                System.out.printf("So witch id " + orderId + "not found in DB");
            }
            List<OrderItem> orderItems = order.getOrderItems();//?
            orderItems.add(orderItem);
            order.setOrderItems(orderItems);
            session.save(order);
            session.getTransaction().commit();
            System.out.println("OrderItem: " + orderItem.getOrderItemId() + " " +  "was added to so " + orderId);

        } finally {
            factoryBuilder.getSessionFactory().close();

        }

    }

    @Override
    public List<Order> getAllOrders() {
        List<Order> orderList;
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            orderList = session.createQuery("FROM Order").list();
        } finally {
            factoryBuilder.getSessionFactory().close();
        }
        return orderList;
    }
}
