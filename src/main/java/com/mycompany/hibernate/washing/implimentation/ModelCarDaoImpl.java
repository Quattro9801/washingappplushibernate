package com.mycompany.hibernate.washing.implimentation;

import com.mycompany.hibernate.washing.DAO.ModelCarDao;
import com.mycompany.hibernate.washing.FactoryBuilder;
import com.mycompany.hibernate.washing.entity.ModelCar;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component("modelCarDao")
public class ModelCarDaoImpl implements ModelCarDao {

    @Autowired
    FactoryBuilder factoryBuilder;

    @Override
    public void addModelCar(ModelCar modelCar) {
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(modelCar);
            System.out.println("Added modelCar" + modelCar);
            session.getTransaction().commit();
        } finally {
            factoryBuilder.getSessionFactory().close();

        }

    }

    @Override
    public void deleteModelCar(int id) {
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            ModelCar modelCar = (ModelCar) session.get(ModelCar.class, id);
            session.delete(modelCar);
            System.out.println("Deleted User " + modelCar);
            session.getTransaction().commit();
        } finally {
            factoryBuilder.getSessionFactory().close();

        }
    }

    @Override
    public List<ModelCar> getAllModelCars() {

        List<ModelCar> modelCars = null;

        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            modelCars = session.createQuery("FROM ModelCar").list();
        } finally {
            factoryBuilder.getSessionFactory().close();
        }
        return modelCars;
    }

    @Override
    public ModelCar getModelCarById(int id) {
        ModelCar modelCar = null;
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            modelCar= (ModelCar) session.get(ModelCar.class, id);
            System.out.println("FindUserByID " + modelCar);
            session.getTransaction().commit();

        } finally {
            factoryBuilder.getSessionFactory().close();
        }
        return modelCar;
    }

    @Override
    public ModelCar updateModelCar(int id, ModelCar updatedModelCar) {
        ModelCar modelCar = null;
        try {
            Session session = factoryBuilder.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            modelCar= (ModelCar) session.get(ModelCar.class, id);
            modelCar.setName(updatedModelCar.getName());
            System.out.println("FindUserByID " + modelCar);
            session.getTransaction().commit();

        } finally {
            factoryBuilder.getSessionFactory().close();
        }

        return modelCar;
    }
}
