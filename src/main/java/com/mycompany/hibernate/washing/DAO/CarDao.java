package com.mycompany.hibernate.washing.DAO;

import com.mycompany.hibernate.washing.entity.Car;

public interface CarDao {
    void createCar(Car car);
    void deleteCar (int id);
    Car updateCar (int id, Car car);
    Car getCarById(int id);
}
