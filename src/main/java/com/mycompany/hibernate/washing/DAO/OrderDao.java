package com.mycompany.hibernate.washing.DAO;

import com.mycompany.hibernate.washing.entity.Order;
import com.mycompany.hibernate.washing.entity.OrderItem;

import java.math.BigInteger;
import java.util.List;

public interface OrderDao {
    void createOrder(Order order);
    Order getOrderById (BigInteger id);
    Order updateOrder(BigInteger id, Order order);
    void addOrderItemToSalesOrder(BigInteger oiId, OrderItem orderItem);
    List<Order> getAllOrders();
}
