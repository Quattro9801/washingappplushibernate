package com.mycompany.hibernate.washing.DAO;

import com.mycompany.hibernate.washing.entity.CarWash;

public interface CarWashDao {
    void createCarWash(CarWash carWash);
    CarWash getCarWashById(int id);
    CarWash updateCarWash(int id, CarWash carWash);
    void deleteCarWash(int id);
}
