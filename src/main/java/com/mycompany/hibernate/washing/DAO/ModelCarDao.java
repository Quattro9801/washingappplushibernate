package com.mycompany.hibernate.washing.DAO;

import com.mycompany.hibernate.washing.entity.ModelCar;

import java.util.List;

public interface ModelCarDao {
    void addModelCar(ModelCar modelCar);
    void deleteModelCar(int id);
    List<ModelCar> getAllModelCars();
    ModelCar getModelCarById(int id);
    ModelCar updateModelCar(int id, ModelCar updatedModelCar);
}
