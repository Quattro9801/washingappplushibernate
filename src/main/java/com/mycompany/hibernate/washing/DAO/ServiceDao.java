package com.mycompany.hibernate.washing.DAO;

import com.mycompany.hibernate.washing.entity.Service;

import java.util.List;

public interface ServiceDao {
    Service addService(Service service);
    void deleteService(int id);
    Service updateService(int id, Service service);
    List<Service> getAllServices();
    Service getServiceById(int id);
}
