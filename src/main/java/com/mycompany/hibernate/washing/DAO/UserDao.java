package com.mycompany.hibernate.washing.DAO;

import com.mycompany.hibernate.washing.entity.Order;
import com.mycompany.hibernate.washing.entity.User;

import java.math.BigInteger;
import java.util.List;

public interface UserDao {
    User addUser(User user);
    String removeUser (int id);
    User findByUserId(int id);
    User updateUserById(int id, User user);
    List<User> getAllUsers();
    List<Order> getAllOrdersByUser(BigInteger userId);
}
