package com.mycompany.hibernate.washing.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "reviews")
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "review_id")
    private int reviewId;
    @Column(name = "content")
    private String content;
    @Column(name = "car_was_id")
    private int carWashId;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "car_wash_id")
    private CarWash carWash;


    public Review(String content, int carWashId) {
        this.content = content;
        this.carWashId = carWashId;
    }

}
