package com.mycompany.hibernate.washing.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "cars" )
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "car_id")
    private int carId;
    @Column(name = "car_name")
    private String carName;
    @Column(name = "model")
    private String model;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "user_id")//аннотация говорит где искать связь между таблицами
    private User user;

    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Order> orders;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "type_car_id")//аннотация говорит где искать связь между таблицами
    private TypeCar typeCar;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "model_id")//аннотация говорит где искать связь между таблицами
    private ModelCar modelCar;

    public Car(String carName, String model, User user, TypeCar typeCar, ModelCar modelCar) {
        this.carName = carName;
        this.model = model;
        this.user = user;
        this.typeCar = typeCar;
        this.modelCar = modelCar;
    }
}
