package com.mycompany.hibernate.washing.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "models_cars")
public class ModelCar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "model_id")
    private int modelId;
    @Column(name = "name")
    private String name;
    @OneToMany(mappedBy = "modelCar", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Car> cars;

    public ModelCar(String name, List<Car> cars) {
        this.name = name;
        this.cars = cars;
    }
}
