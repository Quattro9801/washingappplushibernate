package com.mycompany.hibernate.washing.entity;

import com.mycompany.hibernate.one_to_one.model.Detail;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "car_washes")
public class CarWash {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "car_wash_id")
    private int carWashId;
    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name = "car_wash_name")
    private String carWashName;
    @Column(name = "description")
    private String description;
    @Column(name = "working_hours")
    private String workingHours;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "location_id") // пишем внешний ключ
    private Location location;

    @OneToMany(mappedBy = "carWash", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Order> orders;

    @OneToMany(mappedBy = "carWash", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Review> reviews;


    public CarWash(String phoneNumber, String carWashName, String description, String workingHours, Location location) {
        this.phoneNumber = phoneNumber;
        this.carWashName = carWashName;
        this.description = description;
        this.workingHours = workingHours;
        this.location = location;
    }
}
