package com.mycompany.hibernate.washing.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "type_cars")
public class TypeCar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_id")
    private int typeId;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "typeCar", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Car> cars;

    public TypeCar(String name, String description) {
        this.name = name;
        this.description = description;
    }

}
