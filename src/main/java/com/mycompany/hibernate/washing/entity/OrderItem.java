package com.mycompany.hibernate.washing.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "orderItems")
public class OrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_item_id")
    private int orderItemId;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "order_id")//аннот
    private Order order;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "service_id")
    private Service service;

    public OrderItem(int orderItemId, Order order, Service service) {
        this.orderItemId = orderItemId;
        this.order = order;
        this.service = service;
    }

}
