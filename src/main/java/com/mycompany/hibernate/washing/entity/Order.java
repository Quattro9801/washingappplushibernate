package com.mycompany.hibernate.washing.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private int orderId;
    @Column(name = "status")
    private String status;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "user_id")//аннотация говорит где искать связь между таблицами
    private User user;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "car_id")
    private Car car;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "car_wash_id")
    private CarWash carWash;

    @Column(name = "created_date")
    private Date createdDate;
    @Column(name = "total_price")
    private int totalPrice;
    @OneToMany(mappedBy = "order", cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    private List<OrderItem> orderItems;



    public Order(String status, User user, Car car, CarWash carWash, Date createdDate, int totalPrice) {
        this.status = status;
        this.user = user;
        this.car = car;
        this.carWash = carWash;
        this.createdDate = createdDate;
        this.totalPrice = totalPrice;
    }
}
