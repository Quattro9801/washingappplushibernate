package com.mycompany.hibernate.washing.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "services")
public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "service_id")
    private int serviceId;
    @Column(name = "service_name")
    private String serviceName;
    @Column(name = "service_price")
    private int price;
    @Column(name = "description")
    private String description;
    @OneToOne(mappedBy = "service", cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    private OrderItem orderItem;

    public Service(String serviceName, int price, String description) {
        this.serviceName = serviceName;
        this.price = price;
        this.description = description;
    }


}
