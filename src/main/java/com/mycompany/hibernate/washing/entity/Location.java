package com.mycompany.hibernate.washing.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "locations")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "location_id")
    private int locationId;
    @Column(name = "address_name")
    private String addressName;
    @OneToOne(mappedBy = "location", cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    private CarWash carWash;

    public Location(String addressName) {
        this.addressName = addressName;
    }
}
