package com.mycompany.hibernate.washing.services.userService;

import com.mycompany.hibernate.washing.DAO.UserDao;
import com.mycompany.hibernate.washing.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("userService")
public class UserService {

    @Autowired
    private UserDao userDao;

    public User addUser(User user){
        return userDao.addUser(user);
    }

    public String removeUser(int id){
        return userDao.removeUser(id);
    }

    public User findByUserId(int id){
        return userDao.findByUserId(id);
    }

    public User updateUserById(int id, User user){
        return userDao.updateUserById(id, user);
    }

    public List<User> getAllUsers(){
        return userDao.getAllUsers();
    }
}
