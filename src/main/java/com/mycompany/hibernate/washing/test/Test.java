package com.mycompany.hibernate.washing.test;

import com.mycompany.hibernate.washing.entity.*;
import com.mycompany.hibernate.washing.services.userService.UserService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");//получили applicationContext
        UserService userService = context.getBean("userService", UserService.class);
        userService.findByUserId(1);
        //System.out.println("Before Update" +  userService.findByUserId(1));
        User user = new User("Ivanushkasss123","dmitrii@mail.ru");
        //userService.findByUserId(1);
        userService.updateUserById(1, user);
       // System.out.println("After update" +  user);
      //  User user1 = new User("Dim", "Ivanov", new Date(),"dim@mail.ru","+79108924556");
       // userDao.addUser(user1);
        //System.out.println(userService.getAllUsers());
        context.close();
       /* SessionFactory sessionFactory = new AnnotationConfiguration()// создали фабрику, создаем 1 раз
            .configure("hibernate.cfg.xml")
            .addAnnotatedClass(User.class)
            .addAnnotatedClass(Order.class)
            .addAnnotatedClass(OrderItem.class)
            .addAnnotatedClass(Service.class)
            .addAnnotatedClass(Car.class)
            .addAnnotatedClass(CarWash.class)
            .addAnnotatedClass(TypeCar.class)
            .addAnnotatedClass(Review.class)
            .addAnnotatedClass(Location.class)
                .addAnnotatedClass(ModelCar.class)
            .buildSessionFactory();
        try {
            Session session = sessionFactory.getCurrentSession();
            session.beginTransaction();
            User user = (User) session.get(User.class, 1);
            System.out.println(user);
            session.getTransaction().commit();

        }
        finally {
            sessionFactory.close();

        }*/
    }
}
