package com.mycompany.hibernate.one_to_one.model.test;

import com.mycompany.hibernate.model.Employee;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import javax.persistence.TypedQuery;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        SessionFactory sessionFactory = new AnnotationConfiguration()// создали фабрику, создаем 1 раз
            .configure("hibernate.cfg.xml")
            .addAnnotatedClass(Employee.class)
            .buildSessionFactory();
        try {
           Session session = sessionFactory.getCurrentSession();// получаем сессию
           /*fc v", "L3", 1000000);
            session.beginTransaction();
            session.save(employee);
            session.getTransaction().commit();
            session.close();*/
            session.beginTransaction();
          /*  Employee employee = (Employee) session.get(Employee.class, 1);
            System.out.println(employee);*/
           // Query query = session.createQuery("FROM Movie WHERE title LIKE ?");

           //List<Employee> list = ((List<Employee>) session.createQuery("from Employee "+ "where surname = 'Ivanov'").list());
           List<Employee> list = ((List<Employee>) session.createQuery("from Employee ").list());

   /*         for (Employee em: list) { Вывод всех юзеров
                System.out.println(em);

            }
            session.getTransaction().commit();
            System.out.println("Done");*/
           /* Обновление данных способ 1
            Employee employee = ((Employee) session.get(Employee.class,2));
            employee.setName("Vladimir");
            session.getTransaction().commit();*/
           /*  Обновление 2 способ
            session.createQuery("update Employee set salary = 900000 " + "where name = 'Vladimir'").executeUpdate();
            session.getTransaction().commit();*/
            Employee employeeDel = (Employee) ((Employee) session.get(Employee.class,0));
            session.delete(employeeDel);
            session.getTransaction().commit();

        }
        finally {
            sessionFactory.close();
        }

    }
}