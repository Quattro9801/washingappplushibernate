package com.mycompany.hibernate.one_to_one.model.test;

import com.mycompany.hibernate.model.Detail;
import com.mycompany.hibernate.model.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class OneToOneUniDirect {
    public static void main(String[] args) {
        SessionFactory sessionFactory = new AnnotationConfiguration()// создали фабрику, создаем 1 раз
            .configure("hibernate.cfg.xml")
            .addAnnotatedClass(Employee.class)
            .addAnnotatedClass(Detail.class)
            .buildSessionFactory();
 /*       Employee employee = new Employee("Vas","Smirnov","L2",80000);
        Detail detail = new Detail("Vas@mail.ru", "+79098765656");
        Session session = sessionFactory.getCurrentSession();// получаем сессию*/
        Session session = sessionFactory.getCurrentSession();
        try {
   /*     employee.setDetail(detail);
        session.beginTransaction();
        session.save(employee);
        session.getTransaction().commit();*/


            //получение работника
      /*      session.beginTransaction();
            Employee employee = ((Employee) session.get(Employee.class,67));
            System.out.println(employee.getDetail());
            session.getTransaction().commit();*/


            //удаление
            session.beginTransaction();
            Employee employee = ((Employee) session.get(Employee.class,2));
            session.delete(employee);// благодаря каскаду, удаляется инфа из таблицы details тоже
            session.getTransaction().commit();

    }
    finally {

        session.close();
        sessionFactory.close();
    }
    }

}

