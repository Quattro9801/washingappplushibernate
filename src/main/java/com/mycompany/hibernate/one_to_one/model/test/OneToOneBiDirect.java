package com.mycompany.hibernate.one_to_one.model.test;

import com.mycompany.hibernate.model.Detail;
import com.mycompany.hibernate.model.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class OneToOneBiDirect {
    public static void main(String[] args) {
        SessionFactory sessionFactory = new AnnotationConfiguration()// создали фабрику, создаем 1 раз
            .configure("hibernate.cfg.xml")
            .addAnnotatedClass(Employee.class)
            .addAnnotatedClass(Detail.class)
            .buildSessionFactory();
        Employee employee = new Employee("Alex","Smirnov","L2",80000);
        Detail detail = new Detail("Alex@mail.ru", "+79098765656");
        Session session = sessionFactory.getCurrentSession();// получаем сессию*/
        try {
 /*           session.beginTransaction();
           // employee.setDetail(detail);// убираем если хотим чтобы занчение detail_id не присваивалось
              detail.setEmployee(employee);
              session.save(detail);
              session.getTransaction().commit();*/

            // удаление деталей у работника (Cascade.type не должен быть ALL иначе при удалении деталей мы удалим и employee
       /*     session.beginTransaction();
            Detail detail1 = ((Detail) session.get(Detail.class, 2 )); // получили detail c id =2
            detail1.getEmployee().setDetail(null);// у этого detailId привязанного работника и присовили detail_id =null
            session.delete(detail1);// теперь можем удалить detail, так как работник не ссылается больше
            session.getTransaction().commit();*/

            // удаление работника и деталей вместе
            session.beginTransaction();
            Employee emp2 = ((Employee) session.get(Employee.class, 5));
            session.delete(emp2);
            session.getTransaction().commit();
            System.out.println("Done");
        }
        finally {
           // session.close();
            sessionFactory.close();
        }
    }
}
