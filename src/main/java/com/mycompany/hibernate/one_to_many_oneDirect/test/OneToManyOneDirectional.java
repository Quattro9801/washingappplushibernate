package com.mycompany.hibernate.one_to_many_oneDirect.test;

import com.mycompany.hibernate.one_to_many.modelBiDirect.Order;
import com.mycompany.hibernate.one_to_many.modelBiDirect.User;
import com.mycompany.hibernate.one_to_many_oneDirect.entity.OrderProf;
import com.mycompany.hibernate.one_to_many_oneDirect.entity.UserProfile;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class OneToManyOneDirectional {
    public static void main(String[] args) {
        SessionFactory sessionFactory = new AnnotationConfiguration()// создали фабрику, создаем 1 раз
            .configure("hibernate.cfg.xml")
            .addAnnotatedClass(UserProfile.class)
            .addAnnotatedClass(OrderProf.class)
            .buildSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        try{
           session.beginTransaction();
         /*  Order order = ((Order) session.get(Order.class, 17));
           session.delete(order);*/
          // UserProfile userProfile = ((UserProfile) session.get(UserProfile.class, 10));
            OrderProf orderProf = ((OrderProf) session.get(OrderProf.class, 15));
          //  session.delete(user);
            session.delete(orderProf);
            // System.out.println(userProfile + "////"+ userProfile.getOrders());
            session.getTransaction().commit();

        }finally {
            sessionFactory.close();
        }
    }
}
