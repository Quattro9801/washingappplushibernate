package com.mycompany.hibernate.one_to_many_oneDirect.entity;

import com.mycompany.hibernate.one_to_many.modelBiDirect.Order;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
@Entity
@Table(name = "users")
public class UserProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name")

    private String name;
    @Column(name = "surname")

    private String surname;
    @Column(name = "email")
    private String email;

    @Column(name = "number_phone")
    private String numberPhone;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private List<OrderProf> orders;

    public List<OrderProf> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderProf> orders) {
        this.orders = orders;
    }

    public UserProfile() {
    }

    public UserProfile(String name, String surname, String email, String numberPhone) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.numberPhone = numberPhone;
    }

    @Override
    public String toString() {
        return "User{" +
            "name='" + name + '\'' +
            ", surname='" + surname + '\'' +
            ", email='" + email + '\'' +
            ", numberPhone='" + numberPhone + '\'' +
            '}';
    }
    public void addOrderToUser(OrderProf order){
        if (orders == null){
            orders = new ArrayList<>();
        }
        orders.add(order);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }
}
