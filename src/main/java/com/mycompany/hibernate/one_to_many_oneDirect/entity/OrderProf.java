package com.mycompany.hibernate.one_to_many_oneDirect.entity;

import com.mycompany.hibernate.one_to_many.modelBiDirect.User;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "orders")
public class OrderProf {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "date")
    private String date;
    @Column(name = "status")
    private String status;
    @Column(name = "price")
    private int price;

    public OrderProf(String date, String status, int price) {
        this.date = date;
        this.status = status;
        this.price = price;
    }

    public OrderProf() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Order{" +
            "id=" + id +
            ", date='" + date + '\'' +
            ", status='" + status + '\'' +
            ", price=" + price +
            '}';
    }
}
