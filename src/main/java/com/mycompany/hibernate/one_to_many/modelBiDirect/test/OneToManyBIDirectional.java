package com.mycompany.hibernate.one_to_many.modelBiDirect.test;

import com.mycompany.hibernate.one_to_many.modelBiDirect.Order;
import com.mycompany.hibernate.one_to_many.modelBiDirect.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;


public class OneToManyBIDirectional {
    public static void main(String[] args) {
        SessionFactory sessionFactory = new AnnotationConfiguration()// создали фабрику, создаем 1 раз
            .configure("hibernate.cfg.xml")
            .addAnnotatedClass(User.class)
            .addAnnotatedClass(Order.class)
            .buildSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        try {
            // Добавить в таблицу нового юзера и заказы
       /*   User ivan = new User("Alexey","Ivanov","dmitrii@mail.ru","+89047656767");
            Order order1 = new Order("24.04.2023","Active",890000);
            Order order2 = new Order("24.04.2023","Active",60000);
            session.beginTransaction();
            ivan.addOrderToUser(order1);
            ivan.addOrderToUser(order2);
            session.save(ivan);
            session.getTransaction().commit();*/
          /*  session.beginTransaction();
           // Order order = ((Order) session.get(Order.class, 3));
           // session.delete(order);
            User user = ((User) session.get(User.class, 3));
            session.delete(user);
            session.getTransaction().commit();*/
            session.beginTransaction();
            User user = (User) session.get(User.class, 10);
            System.out.println(user);
            System.out.println("1 заказ + "+ user.getOrders().get(0));
            System.out.println("Done");
            session.getTransaction().commit();
            System.out.println("//////////////////////");
            System.out.println(user.getOrders());

        }finally {
            sessionFactory.close();
        }


    }
}
